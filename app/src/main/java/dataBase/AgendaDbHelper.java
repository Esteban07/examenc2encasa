package dataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class AgendaDbHelper extends SQLiteOpenHelper {

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_USUARIO = " CREATE TABLE " +
            DefinirTabla.Usuario.TABLA_NAME +" ("+
            DefinirTabla.Usuario._ID + " INTEGER PRIMARY KEY, "+
            DefinirTabla.Usuario.COLUMN_NAME_NOMBRE + TEXT_TYPE +
            COMMA_SEP +
            DefinirTabla.Usuario.COLUMN_NAME_CONTRASENA + TEXT_TYPE +
            COMMA_SEP +
            DefinirTabla.Usuario.COLUMN_NAME_CONTRASENA2 + TEXT_TYPE +
            COMMA_SEP +
            DefinirTabla.Usuario.COLUMN_NAME_USUARIO + TEXT_TYPE +
            ")";
    private static final String SQL_DELTE_USUARIO = "DROP TABLE IF EXISTS " +
            DefinirTabla.Usuario.TABLA_NAME;
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "sistema.db";



    public AgendaDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_USUARIO);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db,oldVersion,newVersion);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELTE_USUARIO);
        onCreate(db);
    }
}
